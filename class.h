#pragma once

#include <gtk/gtk.h>
#include <gtk/gtkimmodule.h>

extern "C" {

void RegisterType(GTypeModule *mod);
GtkIMContext* NewContext(GtkIMContext* wrappee);

}