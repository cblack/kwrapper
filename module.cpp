#include <QDebug>
#include <QList>
#include <QDir>
#undef signals
#undef slots
#undef emit

#include <gtk/gtk.h>
#include <gtk/gtkimmodule.h>
#include "class.h"
#include "conf.h"

#include <dlfcn.h>

using IMModuleInit = void(*)(GTypeModule*);
using IMModuleExit = void(*)();
using IMModuleCreate = GtkIMContext*(*)(const gchar*);
using IMModuleList = void(*)(const GtkIMContextInfo ***, gint*);

const char* wrapPrefix = "plasma-";

class Library {
    void* handle;
public:
    IMModuleInit init;
    IMModuleExit exit;
    IMModuleCreate create;
    IMModuleList list;
    QString libPath;

    Library(const char* path) {
        libPath = path;
        handle = dlopen(path, RTLD_LAZY);
        init = getSymbol<IMModuleInit>("im_module_init");
        exit = getSymbol<IMModuleExit>("im_module_exit");
        create = getSymbol<IMModuleCreate>("im_module_create");
        list = getSymbol<IMModuleList>("im_module_list");
    }
    ~Library() {
        dlclose(handle);
    }
    template<typename T>
    T getSymbol(const char* name) {
        auto symbol = dlsym(handle, name);
        return (T) symbol;
    }
};

QList<Library*> Handles() {
    static QList<Library*> handles;
    static bool initted = false;
    if (!initted) {
        initted = true;
        QDir directory(MODULE_PATH);
        QStringList files = directory.entryList(QStringList() << "*.so");
        for (auto item : files) {
            if (item.contains("im-kwrapper")) {
                continue;
            }
            handles << new Library((MODULE_PATH + QStringLiteral("/") + item).toStdString().c_str());
        }
    }
    return handles;
}

extern "C" {

G_MODULE_EXPORT const gchar* g_module_check_init(GModule *module) {
    return NULL;
}

G_MODULE_EXPORT void im_module_init(GTypeModule *module) {
    for (auto handle : Handles()) {
        handle->init(module);
    }
    RegisterType(module);
}

G_MODULE_EXPORT void im_module_exit() {
    for (auto handle : Handles()) {
        handle->exit();
    }
}

G_MODULE_EXPORT GtkIMContext *im_module_create(const gchar *context_id) {
    auto str = std::string(context_id);
    auto qstr = QString::fromStdString(str);
    auto prefQstr = QString::fromStdString(std::string(wrapPrefix));
    if (qstr.startsWith(prefQstr)) {
        qstr = qstr.remove(prefQstr);
    }
    for (auto handle : Handles()) {
        auto item = handle->create(qstr.toStdString().c_str());
        if (item != nullptr) {
            return NewContext(item);
        }
    }
    return nullptr;
}

const char* heapinate(const std::string& source) {
    auto heap = new const char*(source.c_str());
    return *heap;
}

struct Info {
    std::string contextID;
    std::string contextName;
    std::string domain;
    std::string domainDirname;
    std::string defaultLocales;
};

G_MODULE_EXPORT void im_module_list(const GtkIMContextInfo ***contexts, gint *n_contexts) {
    static std::vector<Info> vec;
    static bool initted = false;
    static std::vector<const GtkIMContextInfo*>* retVec = new std::vector<const GtkIMContextInfo*>();

    if (!initted) {
        auto handles = Handles();
        for (auto handle : handles) {
            handle->list(contexts, n_contexts);
            for (int i = 0; i < *n_contexts; i++) {
                auto ctx = *(*contexts[i]);
                auto info = Info {
                    .contextID = ctx.context_id,
                    .contextName = ctx.context_name,
                    .domain = ctx.domain,
                    .domainDirname = ctx.domain_dirname,
                    .defaultLocales = ctx.default_locales
                };

                info.contextID = "plasma-" + info.contextID;
                vec.push_back(info);
            }
        }

        for (const auto& item : vec) {
            auto info = new GtkIMContextInfo;
            info->context_id = item.contextID.c_str();
            info->context_name = item.contextName.c_str();
            info->domain = item.domain.c_str();
            info->domain_dirname = item.domainDirname.c_str();
            info->default_locales = item.defaultLocales.c_str();
            retVec->push_back(info);
        }

        initted = true;
    }

    *contexts = retVec->data();
    *n_contexts = retVec->size();
}

}