#include <QDebug>
#include <PlasmaKeyData/plasmakeydata.h>
#undef signals
#undef slots
#undef emit

#include <functional>
#include "class.h"

extern "C" {

struct KWrapperContext {
    GtkIMContext parent;
    GtkIMContext *wrapped;
    GdkWindow *window;
    GdkRectangle rect;
};

#define kThis ((KWrapperContext*) self)

struct KWrapperContextClass {
    GtkIMContextClass parent;
};

static GType KWrapperContextType = 0;
GType KWrapperContextTypeInstance() {
    if (KWrapperContextType == 0) RegisterType(nullptr);
    return KWrapperContextType;
}

void InitClass (KWrapperContextClass *self);
void DeinitClass (KWrapperContextClass *self);
void InitContext (GObject *obj);

void RegisterType(GTypeModule *mod) {
    const GTypeInfo info = {
        sizeof(KWrapperContextClass),
        nullptr,
        nullptr,
        (GClassInitFunc) InitClass,
        (GClassFinalizeFunc) DeinitClass,
        nullptr,
        sizeof(KWrapperContext),
        0,
        (GInstanceInitFunc) InitContext,
    };
    if (KWrapperContextType == 0) {
        if (mod == nullptr) {
            KWrapperContextType = g_type_register_static(
                GTK_TYPE_IM_CONTEXT,
                "KWrapperContext",
                &info,
                (GTypeFlags)0
            );
        } else {
            KWrapperContextType = g_type_module_register_type(
                mod,
                GTK_TYPE_IM_CONTEXT,
                "KWrapperContext",
                &info,
                (GTypeFlags)0
            );
        }
    }
}

static GtkWidget* window = nullptr;
static bool windowShown = false;

void ConstructWindow(GtkIMContext* self, GdkRectangle* area) {
    static GtkCssProvider* provider = nullptr;
    if (window == nullptr) {
        window = gtk_window_new(GTK_WINDOW_POPUP);

        gtk_widget_realize(window);

        auto handle = gtk_widget_get_window(window);
        gdk_window_set_transient_for(handle, kThis->window);

        gtk_window_set_default_size((GtkWindow*)window, 9, 9);
        gtk_window_move((GtkWindow*)window, area->x, area->y);
        auto style = gtk_widget_get_style_context(window);

        const auto css = R"RJIENRLWEY(
window {
    background-color: @tooltip_background_breeze;
    border-radius: 3px;
    color: @tooltip_text_breeze;
    border: 1px solid @tooltip_border_breeze;
}
.thick {
    font-weight: bold;
}
        )RJIENRLWEY";
        provider = gtk_css_provider_new();
        gtk_css_provider_load_from_data(provider, css, -1, nullptr);
        gtk_style_context_add_provider(style, (GtkStyleProvider*)provider, GTK_STYLE_PROVIDER_PRIORITY_USER);

        gtk_widget_show_all(window);
        windowShown = true;
    }
}

void RepositionWindow(GdkRectangle* area) {
    if (window != nullptr) {
        gtk_window_move((GtkWindow*)window, area->x, area->y+area->height);
    }
}

void ShowWindow() {
    if (window != nullptr && !windowShown) {
        windowShown = true;
        gtk_widget_show_all(window);
    }
}

void HideWindow() {
    if (window != nullptr && windowShown) {
        windowShown = false;
        gtk_widget_hide(window);
    }
}

void SetWindowContents(const QList<QString>& keys) {
    static GtkWidget* previousBox = nullptr;
    GtkWidget* currentBox = nullptr;
    currentBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12);
    int i = 1;
    for (auto key : keys) {
        auto boxlet = gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
        auto topLabel = gtk_label_new(key.toLocal8Bit().data());
        auto bottomLabel = gtk_label_new(QString::number(i).toLocal8Bit().data());

        auto sc = gtk_widget_get_style_context(topLabel);
        gtk_style_context_add_class(sc, "thick");

        gtk_box_pack_start((GtkBox*)boxlet, topLabel, false, false, 0);
        gtk_box_pack_start((GtkBox*)boxlet, bottomLabel, false, false, 0);
        gtk_box_pack_start((GtkBox*)currentBox, boxlet, false, false, 0);

        i++;
    }
    if (previousBox != nullptr) {
        gtk_widget_destroy(previousBox);
    }
    gtk_widget_set_margin_end(currentBox, 6);
    gtk_widget_set_margin_start(currentBox, 6);
    gtk_widget_set_margin_top(currentBox, 6);
    gtk_widget_set_margin_bottom(currentBox, 6);
    gtk_container_add((GtkContainer*)window, currentBox);
    gtk_widget_show_all(currentBox);
    previousBox = currentBox;
}

void InitClass(KWrapperContextClass *self) {
    auto IMCClass = (GtkIMContextClass*) self;
    auto GOBClass = (GObjectClass*) self;

    IMCClass->reset = [](GtkIMContext* self) {
        gtk_im_context_reset(kThis->wrapped);
    };
    IMCClass->focus_in = [](GtkIMContext* self) {
        gtk_im_context_focus_in(kThis->wrapped);
    };
    IMCClass->focus_out = [](GtkIMContext* self) {
        gtk_im_context_focus_out(kThis->wrapped);
    };
    IMCClass->filter_keypress = [](GtkIMContext* self, GdkEventKey *ev) -> gboolean {
        static GdkEventType previousType = (GdkEventType)(0);
        static guint previousVal = (guint)(0);
        static std::string previousAsString;
        static guint previousTime = 0;
        std::string currentSString = std::string(1, (char)gdk_keyval_to_unicode(ev->keyval));
        QString currentQString = QString::fromStdString(currentSString);
        static bool first = true;
        static bool hasRepeated = false;
        bool isRepeat = false;
        int choice = -1;

        if (ev->time <= previousTime) {
            return false;
        }
        previousTime = ev->time;

        if(ev->keyval > 0xf000) {
            previousType = (GdkEventType)(0);
            previousVal = 0;
            previousAsString.clear();
            hasRepeated = false;
            isRepeat = false;
            HideWindow();
            return gtk_im_context_filter_keypress(kThis->wrapped, ev);
        }

        if (ev->window != nullptr && kThis->window == nullptr) {
            kThis->window = ev->window;
        }

        if (first) {
            first = false;
            goto passThrough;
        }

        isRepeat = ((previousType == GDK_KEY_PRESS) && (ev->type == GDK_KEY_PRESS) && (previousVal == ev->keyval));

        if (hasRepeated) {
            if (previousVal == ev->keyval) {
                SetWindowContents(KeyData::KeyMappings[currentQString]);
                ShowWindow();
                return true;
            }
            if (ev->keyval < 0x31 || 0x39 < ev->keyval) {
                goto passThrough;
            }
            choice = ev->keyval - 0x30;
            if (KeyData::KeyMappings[QString::fromStdString(previousAsString)].count() < choice) {
                goto passThrough;
            }
            g_signal_emit_by_name(self, "delete-surrounding", -1, 1, nullptr);
            g_signal_emit_by_name(self, "commit", KeyData::KeyMappings[QString::fromStdString(previousAsString)][choice-1].toLocal8Bit().data());
            hasRepeated = false;
            return true;
        }

        if (isRepeat) {
            if (!KeyData::KeyMappings.contains(currentQString)) {
                goto passThrough;
            }
            if(ev->keyval > 0xf000) {
                goto passThrough;
            }
            ConstructWindow(self, &kThis->rect);
            SetWindowContents(KeyData::KeyMappings[currentQString]);
            hasRepeated = true;
            return true;
        }

    passThrough:
        previousType = ev->type;
        previousVal = ev->keyval;
        previousAsString = std::string(1, (char)gdk_keyval_to_unicode(ev->keyval));
        HideWindow();
        return gtk_im_context_filter_keypress(kThis->wrapped, ev);
    };
    IMCClass->get_preedit_string = [](GtkIMContext *self, gchar **str, PangoAttrList **attrs, gint *cursorPos) {
        gtk_im_context_get_preedit_string(kThis->wrapped, str, attrs, cursorPos);
    };
    IMCClass->set_client_window = [](GtkIMContext *self, GdkWindow *window) {
        kThis->window = window;
        gtk_im_context_set_client_window(kThis->wrapped, window);
    };
    IMCClass->set_cursor_location = [](GtkIMContext *self, GdkRectangle *area) {
        kThis->rect = *area;
        RepositionWindow(area);
        gtk_im_context_set_cursor_location(kThis->wrapped, area);
    };
    IMCClass->set_use_preedit = [](GtkIMContext *self, gboolean usePreedit) {
        gtk_im_context_set_use_preedit(kThis->wrapped, usePreedit);
    };
    IMCClass->set_surrounding = [](GtkIMContext *self, const gchar *text, gint len, gint cursorIndex) {
        gtk_im_context_set_surrounding(kThis->wrapped, text, len, cursorIndex);
    };
    GOBClass->notify = [](GObject *self, GParamSpec *spec) {
        g_object_notify_by_pspec((GObject*)(kThis->wrapped), spec);
    };
    GOBClass->finalize = [](GObject *self) {
        g_object_unref(kThis->wrapped);
    };
}

void DeinitClass(KWrapperContextClass *self) {

}

void InitContext(GObject *obj) {

}

}

template<typename Ret, typename ...Args, typename Callable>
void Connect(const char* sig, GtkIMContext *wrap, KWrapperContext* obj, Callable callable) {
    using functor = Ret (*) (GtkIMContext*, Args..., KWrapperContext*);
    auto ptr = (functor)(callable);
    g_signal_connect(wrap, sig, (GCallback)(ptr), obj);
}

extern "C" {

#pragma optimize( "", off )
#pragma GCC push_options
#pragma GCC optimize ("O0")

__attribute__((optimize("O0"))) GtkIMContext* NewContext(GtkIMContext* wrappee) {
    KWrapperContext* obj = (KWrapperContext*)g_object_new(KWrapperContextTypeInstance(), NULL);
    obj->wrapped = wrappee;

    Connect<void, char*>("commit", wrappee, obj, [](GtkIMContext *target, char* str, KWrapperContext* self) {
        g_signal_emit_by_name(self, "commit", str, nullptr);
    });
    Connect<gboolean, int, int>("delete-surrounding", wrappee, obj, [](GtkIMContext* target, int offset, int charCount, KWrapperContext* self) -> gboolean {
        gboolean ret;
        g_signal_emit_by_name(self, "delete-surrounding", offset, charCount, &ret);
        return ret;
    });
    Connect<void>("preedit-changed", wrappee, obj, [](GtkIMContext* target, KWrapperContext* self) {
        g_signal_emit_by_name(self, "preedit-changed");
    });
    Connect<void>("preedit-end", wrappee, obj, [](GtkIMContext* target, KWrapperContext* self) {
        g_signal_emit_by_name(self, "preedit-end");
    });
    Connect<void>("preedit-start", wrappee, obj, [](GtkIMContext* target, KWrapperContext* self) {
        g_signal_emit_by_name(self, "preedit-start");
    });
    Connect<void>("retrieve-surrounding", wrappee, obj, [](GtkIMContext* target, KWrapperContext* self) {
        qDebug() << ((GObject*)(self))->g_type_instance.g_class;
        g_signal_emit_by_name(self, "retrieve-surrounding");
    });

    return (GtkIMContext*)obj;
}

#pragma GCC pop_options
#pragma optimize( "", on )

}